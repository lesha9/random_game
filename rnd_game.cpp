#include <iostream>
#include <time.h>

using namespace std;

class character //����� ���������
{
private:
	int hp; //�-�� ������
	string name; //��� ���������
	void narrow_damage() //������� ��������� ����� � ����� ��������� (18-25)
	{
		int val = rand() % 7 + 18; //��������� ����� �� 18 �� 25
		hp -= val; //���������� �-�� ������
		cout << name << " ������� " << val << " �����." << endl;
		cout << "� " << name << " �������� " << hp << " HP." << endl;
	}
	void wide_damage() //������� ��������� ����� � ������� ��������� (10-35)
	{
		int val = rand() % 25 + 10;
		hp -= val;
		cout << name << " ������� " << val << " �����." << endl;
		cout << "� " << name << " �������� " << hp << " HP." << endl;
	}
	void heal() //������� ������� � ����� ��������� (18-25)
	{
		int val = rand() % 7 + 18;
		hp += val;
		cout << name << " ������������� " << val << " HP." << endl;
		cout << "� " << name << " �������� " << hp << " HP." << endl;
	}
public:
	character()
	{
		hp = 100;
		name = "default_name";
		//�-�� ������ � ��� �� ���������
	}
	string get_name() //������� �������� �����
	{
		return name;
	}
	void set_name(string s) //������� ��������� �����
	{
		name = s;
	}
	bool action() //�������� ������� ������������ �������
	{
		int rnd = 0;
		if (hp >= 35)
		{
			rnd = rand() % 3; //��������� ����� �� 0 �� 2
			if (rnd == 1) narrow_damage();
			else if (rnd == 2) wide_damage();
			else if (rnd == 0) heal();
			//��������� ����� �������� � ������ 33.(3)%
			return false; //���� ��� �� ���������
		}
		else //���� � ��������� ������ 35 ������
		{
			if (hp > 0)
			{
				rnd = rand() % 4; //rand �� 0 �� 3
				if (rnd == 1) narrow_damage(); //���� 25%
				else if (rnd == 2) wide_damage(); //���� 25%
				else if (rnd == 0 || rnd == 3) heal(); //���� 50%
				return false; //���� �� ��� ���
			}
			else //���� ������ 0 ��� ������
			{
				cout << name << " ��������" << endl;
				return true; //����� ����
			}
		}
	}
};

int main()
{
	setlocale(0, "");
	srand(time(NULL));
	character player, computer; //�������� 2� �������
	player.set_name("Player");
	computer.set_name("Computer"); //����� ��� �����
	bool game_end = false; //���������� ��������� ����
	while (!game_end) //������� ����
	{
		int rnd = rand() % 2;

		if (rnd == 0) game_end = player.action();
		else game_end = computer.action(); //�������� ����� ���� �� �������

		if (game_end == true) break;
	}

	return 0;
}

